import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';

function BicingMapa(props) {

    const posicions = props.dades;

    let marcadors= posicions.map((marca, i) =>{
        return(
            <Marker key={i} position={[marca[0], marca[1]]}>
                <Popup>
                {marca[2]} <br /> Bicis Disponibles: {marca[3]}
                <br/> Slots lliures: {marca[4]}
                </Popup>
            </Marker>
        );
    })

    return (
        <div id="map">
            <MapContainer center={[41.3915514315243, 2.1800440852104668]} zoom={15}>
                <TileLayer
                    attibution='&copy; <a href="https://www.openstretmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" 
                />
                {marcadors}
            </MapContainer>     
        </div>
    );
}

export default BicingMapa;