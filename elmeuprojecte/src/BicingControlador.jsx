import {useEffect, useState} from "react";
import BicingTaula from './BicingTaula.jsx';
import Mapa from './BicingMapa.jsx';
import { Button, Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBicycle } from '@fortawesome/free-solid-svg-icons';


function BicingControlador() {
    const [datos, setDatos] = useState([]);
    const [filtre, setFiltre] = useState("")
    const [cargado, setCargado] = useState(false)
    const element = <FontAwesomeIcon icon={faBicycle} />

    const header = ["Estació", "Bicis disponibles", "Espais lliures", "Latitud", "Longitut"];
    const [posicions, setPosicions] = useState([])
 
    function cargaDatos() {
        fetch("https://api.citybik.es/v2/networks/bicing")
            .then(response => response.json())
            .then(result => {
                if(filtre !== ""){
                    setDatos((result.network.stations).filter(bicis => bicis.free_bikes >= parseInt(filtre)))
                }else{
                    setDatos(result.network.stations)
                }
                // setCargado(true)
            })
            .catch((er) => console.log("Error:" + er))
    }
 
    // useEffect(() => {
    //     cargaDatos();
    // }, [filtrar]);
 
    function omplirfila(bici,i) {
        posicions.push([bici.latitude, bici.longitude, bici.name, bici.free_bikes, bici.empty_slots])

        return (
            <tr key={i}>
                <td>{bici.name}</td>
                <td>{bici.free_bikes}</td>
                <td>{bici.empty_slots}</td>
                <td>{bici.latitude}</td>
                <td>{bici.longitude}</td>
            </tr>
        );
    }
 
    let rows = datos.map((bici, i) => omplirfila(bici,i))
    let err = <h3>No hi ha resultats</h3>
    
    if(datos.length <=0){
        err = <h3>No hi ha resultats</h3>
    }

    return (
        <div>
            <h1>Bicis</h1>
            <Container>
                <div>
                    <Mapa dades={posicions} />
                </div>
                <div className="botonera">
                    <Button className="boto" onClick={cargaDatos}>{element} Cargar bicis </Button>
                    <input className="filtre" type="text" onInput={(e) => setFiltre(e.target.value)} placeholder="Número de bicis" />
                </div>
                <BicingTaula head={header} body={rows} error={err}/>
            </Container>
        </div>
    );
}

export default BicingControlador;