import { Table } from 'react-bootstrap';

function BicingTaula(props) {
 
    let capçal= props.head.map((element, i) => {
        return <th key={i}>{element}</th>
    })

    return (
        <div>
            <Table bordered hover>
                <thead>
                    <tr>
                        {capçal}
                    </tr>
                </thead>
                <tbody>
                    {props.body}
                </tbody>
            </Table>
        </div>
    );
}

 
export default BicingTaula;
