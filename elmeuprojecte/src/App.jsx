import './App.css';
import {useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Controlador from './BicingControlador.jsx';
import 'leaflet/dist/leaflet.css';


function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
        <Controlador />
    </div>
  )
}

export default App;
